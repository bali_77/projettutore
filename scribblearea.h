/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <QColor>
#include <QImage>
#include <QRegion>
#include <QPainter>
#include <QPoint>
#include <QWidget>
#include <QPolygonF>

#include <list>
#include <string.h>
#include<stack>

#include "mainwindow.h"
//using namespace std;


typedef struct{
    QColor myColor;
    int x;
    int y;
}stackColor;


class ScribbleArea : public QWidget
{
    Q_OBJECT

public:
    ScribbleArea(QWidget *parent = 0);

    /* stack functions */
    void clearStack(); /* libere le tas */

    /* reset methode sauf s */
    void resetMethode(char *s); /* remet à zero les options */
    void setTab2d(); /* creer un tableau deux dimensions par rapport à la taille de la fenetre */
    /* methode de slection manuelle , régle les niveaux , pour ne pas avoir plusieurs selections en même temps */
    void manualSelectionModified(bool action);
    bool manualSelectionState();

    /* methode de selection semi manuelle */
    bool semiManualState();
    void semiManualModified(bool action);

    /* methode de selection semi manuelle 2 */
    bool semiManual2State();
    void semiManual2Modified(bool action);

    bool openImage(const QString &fileName);
    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);
    void resetPolygon(); /* remmetre à zero le polygon de la selection manuelle */
    void firstClickActive();
    /* modifier les niveaux de selection de la selection semi manuelle */
    void setHue(int hue);
    void setSaturation(int saturation);
    void setLight(int light);
    void setAlpha(int alpha);

    int getHue();
    int getSaturation();
    int getLight();
    int getAlpha();

    bool isModified() const { return modified; }
    QColor penColor() const { return myPenColor; }
    int penWidth() const { return myPenWidth; }

public slots:
    void clearImage();
    void print();

protected:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

private:
    void drawLineTo(); /* dessine une ligne */
    void resizeImage(QImage *image, const QSize &newSize);
     /* tout ce passe ici lors d'un clic si l'option est active */
    void manualS(QMouseEvent *event);
    void semiManualS(QMouseEvent *event);
    void semiManualS2(QMouseEvent *event);

    /* pour la selection semi manuelle on a la version recursive et avec une pile */
    void recursiveRoad(int h1, int s1 , int l1, int a1 , int nx , int ny);  /* fonctionne aussi sur de petites images */
    void stackRoad(int h1, int s1 , int l1, int a1 , int nx , int ny); /* la meilleur */

    bool modified;
    bool scribbling;
    int myPenWidth;
    bool firstClick = true;
    bool clearArea = false;
    QColor myPenColor;
    QImage image;
    QPoint lastPoint;
    QPoint actualPoint;
    QPoint firstPoint;
    QPolygonF myPolygon;
    /* semi selection rsc */
    std::stack<stackColor> myStack;
    int **tab2d;
    /* seuil hsl */
    int sH = 10; /* seuil hue  */
    int sS = 10; /* seuil saturation */
    int sL = 10; /* ligthness */
    int sA = 50 ; /* seuil transparence alpha */

    /* methode de selection */
    bool manualSelectionActive = false;
    bool semiManualSelectionActive = false;
    bool semiManualSelection2Active = false;

    /* taille de notre zone */
    int maxWidth = 0;
    int maxHeight = 0;

};



#endif
