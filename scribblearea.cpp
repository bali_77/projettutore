/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>
#ifndef QT_NO_PRINTER
#include <QPrinter>
#include <QPrintDialog>
#endif

#include "scribblearea.h"

ScribbleArea::ScribbleArea(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);
    modified = false;
    scribbling = false;
    myPenWidth = 1;
    myPenColor = Qt::blue;
}


int ScribbleArea::getHue(){

    return sH;
}


int ScribbleArea::getSaturation(){

    return sS;
}


int ScribbleArea::getLight(){

    return sL;
}


int ScribbleArea::getAlpha(){

    return sA;
}

void ScribbleArea::setHue(int hue){

    sH = hue;

}

void ScribbleArea::setSaturation(int saturation){

    sS = saturation;
}

void ScribbleArea::setLight(int light){

    sL = light;
}

void ScribbleArea::setAlpha(int alpha){

    sA = alpha;
}

void ScribbleArea::clearStack(){

    while(!myStack.empty()){
        myStack.pop();
    }

}

void ScribbleArea::setTab2d(){

    if( maxWidth != 0){

        for(int i=0; i < maxWidth; i++){
            delete[] tab2d[i];
        }
        delete[] tab2d;
    }

    maxWidth = image.width();
    maxHeight = image.height();

    tab2d = new int*[maxWidth];
    for (int i = 0; i < maxWidth; i++)
       tab2d[i] = new int[maxHeight];

    for(int i = 0 ; i < maxWidth; i++){
        for(int j=0; j < maxHeight; j++){
            tab2d[i][j] = 0;
        }
    }

}

bool ScribbleArea::openImage(const QString &fileName)
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
        return false;

    QSize newSize = loadedImage.size().expandedTo(size());
    resizeImage(&loadedImage, newSize);
    image = loadedImage;
    modified = false;
    update();
    return true;
}

bool ScribbleArea::saveImage(const QString &fileName, const char *fileFormat)
{
    QImage visibleImage = image;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    } else {
        return false;
    }
}

void ScribbleArea::setPenColor(const QColor &newColor)
{
    myPenColor = newColor;
}

void ScribbleArea::setPenWidth(int newWidth)
{
    myPenWidth = newWidth;
}

void ScribbleArea::clearImage()
{
    image.fill(qRgb(255, 255, 255));
    modified = true;
    update();
    clearArea = true;
}

void ScribbleArea::mousePressEvent(QMouseEvent *event)
{
    if( manualSelectionActive == true){
        manualS(&*event);
    }
    else if( semiManualSelectionActive == true){
        semiManualS(&*event);
    }
    else if ( semiManualSelection2Active == true){
        semiManualS2(&*event);
    }


}

void ScribbleArea::semiManualS2(QMouseEvent *event){

    maxWidth = image.width();
    maxHeight = image.height();
    QColor b;
    int posx,posy;

    int h=0, s=0 , l=0 , a=0;


    b = image.pixel(event->pos()); /* couleur rgb */


    if( event->button() == Qt::LeftButton){

        posx = event->x(); /* on part de la */
        posy = event->y(); /* on part de la */

        /* on lance le recursive road */
        b.getHsl(&h,&s,&l,&a);
        stackRoad(h,s,l,a,posx,posy);
        /* ici on reset le tableaux et on dessine par rapport à la couleur souhaitée */

        /**********************************************/

        QPixmap pix(image.width(),image.height());
           pix.fill(Qt::transparent);
           QPainter p(&pix);
           {
               //on sauve l'état du painter
                    p.save();
                   //on positione l'origine du painter dans l'image résultat
                   p.translate(0,0);
                   QPoint pq(0,0);
                   //on déssine la coupe
                   for (int i=0; i < maxWidth; i++){
                       for(int j=0; j < maxHeight; j++){

                           if( tab2d[i][j] == 1){
                               p.setPen(myPenColor);
                               pq.setX(i);
                               pq.setY(j);
                               p.drawPoint(pq);
                               tab2d[i][j] = 0;
                           }
                           else{
                               p.setPen(QColor(image.pixel(i,j)));
                               pq.setX(i);
                               pq.setY(j);
                               p.drawPoint(pq);
                           }

                       }

                   }

               //on restaure l'état du painter
               p.restore();
           }
           image = pix.toImage();

           update();





        /**************************************************/

    }
}


void ScribbleArea::semiManualS(QMouseEvent *event){

    maxWidth = image.width();
    maxHeight = image.height();
    QColor b;
    int posx,posy;

    int h=0, s=0 , l=0 , a=0;


    b = image.pixel(event->pos()); /* couleur rgb */

    if(event->button() == Qt::LeftButton){
                posx = event->x(); /* on part de la */
                posy = event->y(); /* on part de la */

                /* on lance le recursive road */
                b.getHsl(&h,&s,&l,&a);
                /*
                recursiveRoad(h,s,l,a,posx,posy);
                */
                stackRoad(h,s,l,a,posx,posy);

    }
    else if( event->button() == Qt::RightButton){
       /* fin des points de clic ... on lance la separation */
     QPixmap pix(image.width(),image.height());
        pix.fill(Qt::transparent);
        QPainter p(&pix);
        {
            //on sauve l'état du painter
                 p.save();
                //on positione l'origine du painter dans l'image résultat
                p.translate(0,0);
                QPoint pq(0,0);
                //on déssine la coupe
                for (int i=0; i < maxWidth; i++){
                    for(int j=0; j < maxHeight; j++){

                        if( tab2d[i][j] == 1){
                            p.setPen(QColor(image.pixel(i,j)));
                            pq.setX(i);
                            pq.setY(j);
                            p.drawPoint(pq);
                        }
                    }

                }

            //on restaure l'état du painter
            p.restore();
        }
        image = pix.toImage();

        update();
    }

}

void ScribbleArea::stackRoad(int h1, int s1 , int l1, int a1 , int nx , int ny){


    /* condition d'arret */

    if( nx < 0 || nx > maxWidth || ny < 0 || ny > maxHeight){
        return;
    }

    /* on met l'origine dedans */
    stackColor b;
    b.myColor = image.pixel(nx,ny);
    b.x = nx;
    b.y = ny;

    myStack.push(b);

    int h = 0 , s = 0 , l = 0 , a = 0;
    int xx = 0;
    int yy = 0;

    while(!myStack.empty()){

          /* si le premier le reconnait on l'ajoute au tab2d puis on pop les 4 suivants */
          myStack.top().myColor.getHsl(&h,&s,&l,&a);
          xx = myStack.top().x;
          yy = myStack.top().y;

           if( tab2d[xx][yy] != 1 && (h < (h1+sH) && h > (h1-sH))
            &&  (s < (s1+sS) && s > (s1-sS))
            &&  (l < (l1+sL) && l > (l1-sL))
            && (a < (a1+sA) && a > (a1-sA)) ){
               /* si la couleur correspond on le met dans tableau puis on insere
                *  4 autres coordonnees ... */
                  tab2d[xx][yy]= 1;
                  /* on enleve le top */
                  myStack.pop();

                  /* on ajoute le reste */
                  if( xx+1 < maxWidth){
                  b.myColor = image.pixel(xx+1,yy);
                  b.x = xx+1;
                  b.y = yy;

                  myStack.push(b);
                  }

                  /* on ajoute le reste */
                  if( xx-1 > -1){
                  b.myColor = image.pixel(xx-1,yy);
                  b.x = xx-1;
                  b.y = yy;

                  myStack.push(b);
                  }

                  /* on ajoute le reste */
                  if( yy+1 < maxHeight){
                  b.myColor = image.pixel(xx,yy+1);
                  b.x = xx;
                  b.y = yy+1;

                  myStack.push(b);
                  }

                  /* on ajoute le reste */
                  if( yy-1 > -1){
                  b.myColor = image.pixel(xx,yy-1);
                  b.x = xx;
                  b.y = yy-1;

                  myStack.push(b);
                  }

           }
           else{
                  myStack.pop();

            }



    }


    return;
}

void ScribbleArea::recursiveRoad(int h1, int s1 , int l1, int a1 , int nx , int ny){

    /* condition d'arret */

    if( nx < 0 || nx > maxWidth || ny < 0 || ny > maxHeight){
        return;
    }

    QColor b = image.pixel(nx,ny);
    int h = 0, s = 0 , l = 0;
    int a = 0;

    b.getHsl(&h,&s,&l,&a);

              if( tab2d[nx][ny] != 1 && (h < (h1+sH) && h > (h1-sH)) && (s < (s1+sS) && s > (s1-sS))  &&  (l < (l1+sL) && l > (l1-sL))  && (a < (a1+sA) && a > (a1-sA)) ){

                    tab2d[nx][ny] = 1;
                    recursiveRoad(h1,s1,l1,a1,nx+1,ny);
                     recursiveRoad(h1,s1,l1,a1,nx-1,ny);
                      recursiveRoad(h1,s1,l1,a1,nx,ny+1);
                       recursiveRoad(h1,s1,l1,a1,nx,ny-1);
                       /* + 4 */
                        /* recursiveRoad(origine,nx+1,ny+1);
                          recursiveRoad(origine,nx-1,ny-1);
                           recursiveRoad(origine,nx-1,ny+1);
                            recursiveRoad(origine,nx+1,ny-1);*/
                }


    return;

}

void ScribbleArea::manualS(QMouseEvent *event){

    if (event->button() == Qt::LeftButton) {

        if(firstClick == true || clearArea == true){
           actualPoint = event->pos();
           firstPoint = event->pos();
           firstClick = false;
           clearArea = false;

           /* on reset le polygon */
           myPolygon = QPolygonF(0);
           myPolygon << actualPoint;

        }
        else{
        lastPoint = actualPoint;
        actualPoint = event->pos();
        scribbling = true;
        drawLineTo();
        myPolygon << actualPoint;
        scribbling = false;
        }
    }
    else if(event->button() == Qt::RightButton ){
        /* si on fait clic gauche on ferme le polygon */
            myPolygon << firstPoint;
            lastPoint = firstPoint;
            drawLineTo();
            /* on dessine notre polygone et ce qu'il contient */
            QPixmap pix(image.width(),image.height());
            pix.fill(Qt::transparent);
            QPainter p(&pix);
            {
                    //on sauve l'état du painter
                    p.save();
                    //on enlève le pen par défaut
                    p.setPen(Qt::NoPen);
                    //on définie le brush utilisé pour remplire l'intèrieur de la zone
                    p.setBrush(image);
                    //on positione l'origine du painter dans l'image résultat
                    p.translate(0,0);
                    //on déssine la coupe
                    p.drawPolygon(myPolygon);
                    //on restaure l'état du painter
                    p.restore();
            }
            image = pix.toImage();
            update();
            myPolygon = QPolygonF(0);
            firstClick = true;






    }
}

void ScribbleArea::resetMethode(char *s){

    if(strcmp(s,"manualSelectionActive") == 0){
        semiManualSelectionActive = false;
        semiManualSelection2Active = false;
    }
    else if ( strcmp(s,"semiManualSelectionActive") == 0){
        manualSelectionActive = false;
        semiManualSelection2Active = false;
    }
    else if ( strcmp(s,"semiManualSelection2Active") == 0 ){
        semiManualSelectionActive = false;
        manualSelectionActive = false;
    }

}

void ScribbleArea::semiManual2Modified(bool action){
    if(action == true){
        //get other false
        resetMethode("semiManualSelection2Active");

        semiManualSelection2Active = true;
    }
    else{
        semiManualSelection2Active = false;
    }


}

void ScribbleArea::manualSelectionModified(bool action){
    if(action == true){
        //get other false
        resetMethode("manualSelectionActive");

        manualSelectionActive = true;
    }
    else{
        manualSelectionActive = false;
    }


}

void ScribbleArea::semiManualModified(bool action){
    if(action == true){
        //get other false
        resetMethode("semiManualSelectionActive");

        semiManualSelectionActive = true;
    }
    else{
        semiManualSelectionActive = false;
    }


}

bool ScribbleArea::semiManualState(){
    return semiManualSelectionActive;
}

bool ScribbleArea::semiManual2State(){
    return semiManualSelection2Active;
}

bool ScribbleArea::manualSelectionState(){
    return manualSelectionActive;
}



void ScribbleArea::resetPolygon(){
    myPolygon = QPolygonF(0);
}

void ScribbleArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);
}

void ScribbleArea::firstClickActive(){
    firstClick = true;
}

void ScribbleArea::resizeEvent(QResizeEvent *event)
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}

void ScribbleArea::drawLineTo()
{
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, actualPoint);
    modified = true;

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, actualPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
}

void ScribbleArea::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_RGB32);
    newImage.fill(qRgb(255, 255, 255));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

void ScribbleArea::print()
{
#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
    QPrinter printer(QPrinter::HighResolution);

    QPrintDialog printDialog(&printer, this);
    if (printDialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = image.size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(image.rect());
        painter.drawImage(0, 0, image);
    }
#endif // QT_NO_PRINTER
}

