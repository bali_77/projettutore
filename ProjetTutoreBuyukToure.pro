#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T13:07:08
# TOURE BUYUK
#
#-------------------------------------------------
QT += widgets
qtHaveModule(printsupport): QT += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjetTutoreBuyukToure
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        scribblearea.cpp

HEADERS  += mainwindow.h \
    scribblearea.h

FORMS    += mainwindow.ui

RESOURCES += \
    ressource.qrc
