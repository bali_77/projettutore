/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>

#include "mainwindow.h"
#include "scribblearea.h"

MainWindow::MainWindow()
{
    scribbleArea = new ScribbleArea;
    setCentralWidget(scribbleArea);

    createActions();
    createMenus();

    setWindowTitle(tr("ProjetTutoreBuyukToure"));
    resize(500, 500);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::open()
{
    /* reset tab && stack */
    scribbleArea->setTab2d();
    scribbleArea->clearStack();

    if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this,
                                   tr("Open File"), QDir::currentPath());
        if (!fileName.isEmpty())
            scribbleArea->openImage(fileName);
    }
}

void MainWindow::save()
{
    QAction *action = qobject_cast<QAction *>(sender());
    QByteArray fileFormat = action->data().toByteArray();
    saveFile(fileFormat);
}

void MainWindow::penColor()
{
    QColor newColor = QColorDialog::getColor(scribbleArea->penColor());
    if (newColor.isValid())
        scribbleArea->setPenColor(newColor);
}

void MainWindow::penWidth()
{
    bool ok;
    int newWidth = QInputDialog::getInt(this, tr("Scribble"),
                                        tr("Select pen width:"),
                                        scribbleArea->penWidth(),
                                        1, 50, 1, &ok);
    if (ok)
        scribbleArea->setPenWidth(newWidth);
}

void MainWindow::hslaParametre(){

    bool ok;
    QString label;
    label.resize(50);
    char tmp[50];

    sprintf(tmp,"Select hue level , default %d :", scribbleArea->getHue());
    label.replace(0,49,tmp);

    int newHue =  QInputDialog::getInt(this, tr("HSLA"),
                                       label,
                                       scribbleArea->penWidth(),
                                       0, 255, 5, &ok);
    if(ok)
        scribbleArea->setHue(newHue);

    sprintf(tmp,"Select saturation level , default %d :", scribbleArea->getSaturation());
    label.replace(0,49,tmp);

    newHue =  QInputDialog::getInt(this, tr("HSLA"),
                                       label,
                                       scribbleArea->penWidth(),
                                       0, 255, 5, &ok);
    if(ok)
        scribbleArea->setSaturation(newHue);

    sprintf(tmp,"Select light level , default %d :", scribbleArea->getLight());
    label.replace(0,49,tmp);

    newHue =  QInputDialog::getInt(this, tr("HSLA"),
                                       label,
                                       scribbleArea->penWidth(),
                                       0, 255, 5, &ok);
    if(ok)
        scribbleArea->setLight(newHue);

    sprintf(tmp,"Select alpha level , default %d :", scribbleArea->getAlpha());
    label.replace(0,49,tmp);

    newHue =  QInputDialog::getInt(this, tr("HSLA"),
                                       label,
                                       scribbleArea->penWidth(),
                                       0, 255, 5, &ok);
    if(ok)
        scribbleArea->setAlpha(newHue);
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Scribble"),
            tr("<p>The <b>Projet Tutoré BUYUK TOURE</b> projet en option , 2015/2016 License INFO L3 / Semestre 5 "
               " Basé sur le sribble ( exemple QT ), on a essayé d'implémenter plusieurs selections objet dans une image </br> "
               " Fait en C++/Qt </br> "
               ".</p>"));
}

void MainWindow::helpManualS(){
    QMessageBox::about(this, tr("Selection Manuelle ?"),
            tr("<p> Activer la selection à partir du menu option</p>"
               "<p>Puis si le pouce est visible commencer la délimitation</p>"
               "<p>Clic droit pour les tracés puis pour fermer la selection clic gauche ...</p>"
               "<p>Et normalement il ne devra rester que la selection dans la fenetre</p>"));

}

void MainWindow::helpSemiSelection2(){
    QMessageBox::about(this, tr("Selection Semi Manuelle 2 ?"),
            tr("<p> Activer la selection à partir du menu option</p>"
               "<p>Modifier la couleur dans pen color </p>"
               "<p>Clic droit pour les zones que l'on veut colorier</p>"
               "<p>Les parametres de sélécitons sont disponible dans hsla parametre </p>"
               "<p>La sélection va prendre la couleur que l'on aura réglée.</p>"));

}

void MainWindow::helpSemiSelection(){
    QMessageBox::about(this, tr("Selection Semi Manuelle ?"),
            tr("<p> Activer la selection à partir du menu option</p>"
               "<p>Puis si le pouce est visible commencer la séléction de zone de couleurs</p>"
               "<p>Clic droit pour les zones que l'on veut puis pour fermer la selection clic gauche ...</p>"
               "<p>Les parametres de séléctino peuvent être modfié dans le menu option... </p>"
               "<p>Et normalement il ne devra rester que la selection dans la fenetre</p>"));

}

void MainWindow::createActions()
{
    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    foreach (QByteArray format, QImageWriter::supportedImageFormats()) {
        QString text = tr("%1...").arg(QString(format).toUpper());

        QAction *action = new QAction(text, this);
        action->setData(format);
        connect(action, SIGNAL(triggered()), this, SLOT(save()));
        saveAsActs.append(action);
    }

    printAct = new QAction(tr("&Print..."), this);
    connect(printAct, SIGNAL(triggered()), scribbleArea, SLOT(print()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    penColorAct = new QAction(tr("&Pen Color..."), this);
    connect(penColorAct, SIGNAL(triggered()), this, SLOT(penColor()));

    penWidthAct = new QAction(tr("Pen &Width..."), this);
    connect(penWidthAct, SIGNAL(triggered()), this, SLOT(penWidth()));

    clearScreenAct = new QAction(tr("&Clear Screen"), this);
    clearScreenAct->setShortcut(tr("Ctrl+L"));
    connect(clearScreenAct, SIGNAL(triggered()),
            scribbleArea, SLOT(clearImage()));

    helpManualSelec = new QAction(tr("&Aide Selection Manuelle"), this);
    connect(helpManualSelec,SIGNAL(triggered()),this, SLOT(helpManualS()));

    helpSemiSelec = new QAction(tr("&Aide Selection Semi Manuelle"), this);
    connect(helpSemiSelec,SIGNAL(triggered()),this, SLOT(helpSemiSelection()));

    helpSemiSelec2 = new QAction(tr("&Aide Selection Semi Manuelle 2"), this);
    connect(helpSemiSelec2,SIGNAL(triggered()),this, SLOT(helpSemiSelection2()));

    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    hslaPara = new QAction(tr("HSLA &Parametre..."), this);
    connect(hslaPara, SIGNAL(triggered()), this, SLOT(hslaParametre()));



    /* methode de selection */
    manualSelect = new QAction(tr("Manual Selection..."), this);
    connect(manualSelect, SIGNAL(triggered()), this, SLOT(manualSelection()));

    semiManualSelect = new QAction(tr("Semi Manual Selection..."), this);
    connect(semiManualSelect, SIGNAL(triggered()), this, SLOT(semiManualSelection()));

    semiManualSelect2 = new QAction(tr("Semi Manual Selection 2..."), this);
    connect(semiManualSelect2, SIGNAL(triggered()), this, SLOT(semiManualSelection2()));
}



void MainWindow::createMenus()
{
    saveAsMenu = new QMenu(tr("&Save As"), this);
    foreach (QAction *action, saveAsActs)
        saveAsMenu->addAction(action);

    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(openAct);
    fileMenu->addMenu(saveAsMenu);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    optionMenu = new QMenu(tr("&Options"), this);
    optionMenu->addAction(penColorAct);
    optionMenu->addAction(penWidthAct);
    optionMenu->addAction(hslaPara);
    /* methode sleection */
    optionMenu->addAction(manualSelect);
    optionMenu->addAction(semiManualSelect);
    optionMenu->addAction(semiManualSelect2);

    optionMenu->addSeparator();
    optionMenu->addAction(clearScreenAct);

    helpMenu = new QMenu(tr("&Help"), this);
    helpMenu->addAction(helpManualSelec);
    helpMenu->addAction(helpSemiSelec);
    helpMenu->addAction(helpSemiSelec2);
    helpMenu->addSeparator();
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(optionMenu);
    menuBar()->addMenu(helpMenu);

    // add icon
    //manualSelect->setIcon(QIcon("\\Users\\xxx\\Documents\\ProjetTutoreBuyukToure\\img\\ok.png"));
    manualSelect->setIcon(QIcon(":\\img\\ok.png"));
    manualSelect->setIconVisibleInMenu(false);

    semiManualSelect->setIcon(QIcon(":\\img\\ok.png"));
    semiManualSelect->setIconVisibleInMenu(false);

    semiManualSelect2->setIcon(QIcon(":\\img\\ok.png"));
    semiManualSelect2->setIconVisibleInMenu(false);
}


void MainWindow::semiManualSelection2(){
    if( (scribbleArea->semiManual2State()) == false){
        scribbleArea->clearStack();
        scribbleArea->setTab2d();
        scribbleArea->semiManual2Modified(true);
        scribbleArea->firstClickActive();
        semiManualSelect2->setIconVisibleInMenu(true);
        /* get other false */
        manualSelect->setIconVisibleInMenu(false);
        semiManualSelect->setIconVisibleInMenu(false);


    }
    else{
        scribbleArea->semiManualModified(false);
        scribbleArea->clearStack();
        scribbleArea->setTab2d();

        /* get toher false */
        semiManualSelect->setIconVisibleInMenu(false);
        manualSelect->setIconVisibleInMenu(false);

    }

}

void MainWindow::semiManualSelection(){
    if( (scribbleArea->semiManualState()) == false){
        scribbleArea->clearStack();
        scribbleArea->setTab2d();
        scribbleArea->semiManualModified(true);
        scribbleArea->firstClickActive();
        semiManualSelect->setIconVisibleInMenu(true);
        /* get other false */
        manualSelect->setIconVisibleInMenu(false);
        semiManualSelect2->setIconVisibleInMenu(false);

    }
    else{
        scribbleArea->semiManualModified(false);
        scribbleArea->clearStack();
        scribbleArea->setTab2d();
        /* get other false */
        semiManualSelect->setIconVisibleInMenu(false);
        semiManualSelect2->setIconVisibleInMenu(false);

    }

}



void MainWindow::manualSelection(){
    if( (scribbleArea->manualSelectionState()) == false){
        scribbleArea->manualSelectionModified(true);
        scribbleArea->resetPolygon();
        scribbleArea->firstClickActive();
        manualSelect->setIconVisibleInMenu(true);

        /* get other false */
        semiManualSelect->setIconVisibleInMenu(false);
        semiManualSelect2->setIconVisibleInMenu(false);

    }
    else{
        scribbleArea->manualSelectionModified(false);
        scribbleArea->resetPolygon();
        /* get other false */
        manualSelect->setIconVisibleInMenu(false);
        semiManualSelect2->setIconVisibleInMenu(false);

    }

}

bool MainWindow::maybeSave()
{
    if (scribbleArea->isModified()) {
       QMessageBox::StandardButton ret;
       ret = QMessageBox::warning(this, tr("Scribble"),
                          tr("The image has been modified.\n"
                             "Do you want to save your changes?"),
                          QMessageBox::Save | QMessageBox::Discard
                          | QMessageBox::Cancel);
        if (ret == QMessageBox::Save) {
            return saveFile("png");
        } else if (ret == QMessageBox::Cancel) {
            return false;
        }
    }
    return true;
}

bool MainWindow::saveFile(const QByteArray &fileFormat)
{
    QString initialPath = QDir::currentPath() + "/untitled." + fileFormat;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                               initialPath,
                               tr("%1 Files (*.%2);;All Files (*)")
                               .arg(QString::fromLatin1(fileFormat.toUpper()))
                               .arg(QString::fromLatin1(fileFormat)));
    if (fileName.isEmpty()) {
        return false;
    } else {
        return scribbleArea->saveImage(fileName, fileFormat.constData());
    }
}
